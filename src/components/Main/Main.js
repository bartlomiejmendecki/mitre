import { CircularProgress } from '@mui/material'
import PropTypes from 'prop-types';
import Navigation from '../Navigation/Navigation';
import Search from '../Search/Search';
import View from '../View/View';
import styles from './Main.module.scss';

const Main = ({ isLoading, data }) => {
    return (
        isLoading ? <div className={styles.loader}><CircularProgress /></div> :
            <div className={styles.wrapper}>
                <Search />
                <Navigation categories={data.categories}/>
                <View navigator={data.navigator} />
            </div>);
}

Main.propTypes = {
    isLoading: PropTypes.bool
}

export default Main;