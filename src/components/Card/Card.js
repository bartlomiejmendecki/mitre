import { Context } from "../../lib/Context";
import PropTypes from "prop-types";
import { useContext, useState } from "react";
import styles from "./Card.module.scss";
import classNames from "clsx";
import InfoIcon from "@mui/icons-material/Info";;

const Card = ({ technique }) => {
  const { dataObject, setDataObject } = useContext(Context);
  const [value, setValue] = useState(0);

  const handleClick = () => {
    console.log("dataObject", dataObject);
    if (!dataObject[technique.id]) {
      const o = Object.assign(dataObject, {
        [technique.id]: { clicked: true, colorShade: 1 },
      });
      setDataObject(o);
      setValue(value + 1);
    } else {
      const cs = dataObject[technique.id].colorShade;
      const updated = cs >= 5 ? 0 : cs + 1;
      const o = Object.assign(dataObject, {
        [technique.id]: { clicked: true, colorShade: updated },
      });
      setDataObject(o);
      setValue(value + 1);
    }
  };

  return (
    <div
      className={classNames(
        styles.wrapper,
        styles[`shade${dataObject[technique.id]?.colorShade || 0}`]
      )}
      onClick={handleClick}
    >
      <p>{technique.name}</p>
      <InfoIcon className={styles.iconInfo} />
    </div>
  );
};

Card.propTypes = {
  technique: PropTypes.object.isRequired,
};

export default Card;
