import PropTypes from "prop-types";
import { useContext } from "react";
import { Context } from "../../lib/Context";
import Column from "../Column/Column";
import styles from "./View.module.scss";

const View = ({ navigator }) => {
  console.log('all', navigator);
  const { category } = useContext(Context);
  const filterCat = (tactic) => {
        return category === 0 ? tactic : tactic.category_id === category
  }
  return (
    <div className={styles.wrapper}>
      {navigator.map((item) => {
        const data = item.tactics.filter(filterCat);
        return data.length ? (
          <div className={styles.panel} key={item.id}>
            <h3>{item.name}</h3>
            <div className={styles.columns}>
                {data.map(tactic => {
                    return <Column key={tactic.id} tactic={tactic} />;
                })}
            </div>
          </div>
        ) : null;
      })}
    </div>
  );
};

View.propTypes = {
  category: PropTypes.number.isRequired,
};

export default View;
