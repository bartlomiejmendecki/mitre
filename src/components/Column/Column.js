
import Card from "../Card/Card";
import PropTypes from "prop-types";
import styles from './Column.module.scss';
import { useContext } from "react";
import { Context } from "../../lib/Context";

const Column = ({ tactic }) => {
  const { phrase } = useContext(Context);
  const filterTech = (tech) => {
    if (phrase) {
        return tech.name.startsWith(phrase);
    }
    return tech
  }

  return (
    <div className={styles.column}>
      <h4>{tactic.name}</h4>
      {tactic.techniques
        .filter(filterTech)
        .map(tech => <Card key={tech.id} technique={tech} />)}
    </div>
  );
};

Column.propTypes = {
  tactic: PropTypes.object,
};

export default Column;
