import classNames from 'clsx';
import { useContext, useEffect, useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import styles from './Navigation.module.scss';
import { Context } from '../../lib/Context';
import { CheckBox, FilterList, OpenWith } from '../../lib/Icons/Icons';


const reorder = (list, startIndex, endIndex) => {
    const result = [...list];
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const getItemStyle = (isDragging, draggableStyle) => ({
    background: isDragging ? '#ffffff' : '#ededed',
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? '#e6e6e6' : '#ededed',
});



const Navigation = ({ categories }) => {
    const { category, setCategory } = useContext(Context);
    const [items, setItems] = useState([]);

    useEffect(() => {
        setItems(categories);
    }, [categories]);

    const handleClick = (v) => {
        console.log(v);
        setCategory(v);
    }

    const onDragEnd = (result) => {
        if (!result.destination) {
            return;
        }
        const reordered = reorder(
            items,
            result.source.index,
            result.destination.index
        );
        setItems(reordered);
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable" direction="horizontal">
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        className={styles.list}
                        style={getListStyle(snapshot.isDraggingOver)}
                        {...provided.droppableProps}
                    >
                        {items.map((item, index) => (
                            <Draggable key={item.id} draggableId={'' + item.id} index={index}>
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        className={classNames(
                                            styles.listItem,
                                            {[styles.active] : category === item.id}
                                        )}
                                        style={getItemStyle(
                                            snapshot.isDragging,
                                            provided.draggableProps.style
                                        )}
                                        onClick={() => handleClick(item.id)}
                                    >
                                        <OpenWith />
                                        <span>{item.name}</span>
                                        <CheckBox />
                                        <span>{ [2,3].includes(item.id) && <FilterList /> }</span>
                                    </div>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    );
}

export default Navigation;

