import { IconButton, InputAdornment, TextField } from "@mui/material";
import { debounce } from "lodash";
import { useContext, useRef } from "react";
import { Context } from "../../lib/Context";
import SearchIcon from "@mui/icons-material/Search";

const Search = () => {
    const { setPhrase } = useContext(Context);
    const delayedSearch = useRef(
        debounce((value) => {
            setPhrase(value)
        }, 300)
    ).current;

    const handleChange = (e) => {
        delayedSearch(e.target.value);
    }
    return (
        <TextField
            label="Search"
            type="search"
            variant="standard"
            onChange={handleChange}
            InputProps={{
                endAdornment: (
                    <InputAdornment>
                        <IconButton>
                            <SearchIcon />
                        </IconButton>
                    </InputAdornment>
                )
              }}
        />)
    // return <input  />
}

export default Search;